import 'package:register_demo/register/model/index.dart';

Future<UserInfo?> registerData(email) {
  return Future.delayed(const Duration(milliseconds: 200), () {
    if (email != 'asd@gmail.com') {
      return UserInfo(email: email, id: 1);
    } else {
      return null;
    }
  });
}
