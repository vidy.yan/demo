part of 'app_bloc.dart';

abstract class AppEvent extends Equatable {
  const AppEvent();

  @override
  List<Object> get props => [];
}

// 修改email
class ChangeEmailText extends AppEvent {
  const ChangeEmailText(this.text);
  final String text;

  @override
  List<Object> get props => [text];
}
