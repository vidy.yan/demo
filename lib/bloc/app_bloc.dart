import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:equatable/equatable.dart';

part 'app_event.dart';
part 'app_state.dart';

class AppBloc extends Bloc<AppEvent, AppState> {
  AppBloc() : super(AppState.initial()) {
    on<ChangeEmailText>(_changeEmailText);
  }

  // 修改用户email
  void setEmailText(text) {
    add(ChangeEmailText(text));
  }

  void _changeEmailText(ChangeEmailText event, Emitter<AppState> emit) {
    emit(state.copyWith(
      emailText: event.text,
    ));
  }
}
