part of 'app_bloc.dart';

class AppState extends Equatable {
  const AppState({
    required this.emailText,
  });
  final String emailText;
  @override
  List<Object> get props => [
        emailText,
      ];

  factory AppState.initial() => const AppState(
        emailText: '',
      );

  @override
  bool get stringify => true;

  get isNotEmpty => null;

  AppState copyWith({
    String? emailText,
  }) {
    return AppState(
      emailText: emailText ?? this.emailText,
    );
  }
}
