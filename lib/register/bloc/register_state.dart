part of 'register_bloc.dart';

class RegisterState extends Equatable {
  const RegisterState({
    required this.emailText,
    required this.passwordText,
    required this.confirmPasswordText,
    required this.canNext,
    required this.showDialogTag,
  });
  final String emailText;
  final String passwordText;
  final String confirmPasswordText;
  final bool canNext;
  final bool showDialogTag;
  @override
  List<Object> get props => [
        emailText,
        passwordText,
        confirmPasswordText,
        canNext,
        showDialogTag,
      ];

  factory RegisterState.initial() => const RegisterState(
        emailText: '',
        passwordText: '',
        confirmPasswordText: '',
        canNext: false,
        showDialogTag: false,
      );

  @override
  bool get stringify => true;

  get isNotEmpty => null;

  RegisterState copyWith({
    String? emailText,
    String? passwordText,
    String? confirmPasswordText,
    bool? canNext,
    bool? showDialogTag,
  }) {
    return RegisterState(
      emailText: emailText ?? this.emailText,
      passwordText: passwordText ?? this.passwordText,
      confirmPasswordText: confirmPasswordText ?? this.confirmPasswordText,
      canNext: canNext ?? this.canNext,
      showDialogTag: showDialogTag ?? this.showDialogTag,
    );
  }
}
