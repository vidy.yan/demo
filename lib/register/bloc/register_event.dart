part of 'register_bloc.dart';

abstract class RegisterEvent extends Equatable {
  const RegisterEvent();

  @override
  List<Object> get props => [];
}

// 修改email
class ChangeEmailText extends RegisterEvent {
  const ChangeEmailText(this.text);
  final String text;

  @override
  List<Object> get props => [text];
}

// 设置弹窗
class ChangeShowDialogTag extends RegisterEvent {
  const ChangeShowDialogTag(this.tag);
  final bool tag;

  @override
  List<Object> get props => [tag];
}

// 修改password
class ChangePasswordText extends RegisterEvent {
  const ChangePasswordText(this.text);
  final String text;

  @override
  List<Object> get props => [text];
}

// 修改确认密码
class ChangeConfirmPasswordText extends RegisterEvent {
  const ChangeConfirmPasswordText(this.text);
  final String text;

  @override
  List<Object> get props => [text];
}

// 检查
class CheckNext extends RegisterEvent {
  const CheckNext();
}

// 注册
class GoRegister extends RegisterEvent {
  const GoRegister(this.context, this.navigator);
  final BuildContext context;
  final navigator;

  @override
  List<Object> get props => [context, navigator];
}
