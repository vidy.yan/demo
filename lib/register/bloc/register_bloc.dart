import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:register_demo/utils/mock.dart';
import 'package:register_demo/home/view/home_page.dart';
import 'package:register_demo/register/model/index.dart';

part 'register_event.dart';
part 'register_state.dart';

class RegisterBloc extends Bloc<RegisterEvent, RegisterState> {
  RegisterBloc() : super(RegisterState.initial()) {
    on<ChangeEmailText>(_changeEmailText);
    on<ChangePasswordText>(_changePassword);
    on<ChangeConfirmPasswordText>(_changeConfirmPassword);
    on<CheckNext>(_checkNext);
    on<GoRegister>(_goRegister);
    on<ChangeShowDialogTag>(_changeShowDialogTag);
  }

  // 修改用户email
  void setEmailText(text) {
    add(ChangeEmailText(text));
  }

  void _changeEmailText(ChangeEmailText event, Emitter<RegisterState> emit) {
    emit(state.copyWith(
      emailText: event.text,
    ));
    checkText();
  }

  // 修改用户密码
  void setPassword(text) {
    add(ChangePasswordText(text));
  }

  void _changePassword(ChangePasswordText event, Emitter<RegisterState> emit) {
    emit(state.copyWith(
      passwordText: event.text,
    ));
    checkText();
  }

  // 确认密码
  void setConfirmPassword(text) {
    add(ChangeConfirmPasswordText(text));
  }

  void _changeConfirmPassword(
      ChangeConfirmPasswordText event, Emitter<RegisterState> emit) {
    emit(state.copyWith(
      confirmPasswordText: event.text,
    ));
    checkText();
  }

  // 检测是否可以直接注册
  void checkText() {
    add(const CheckNext());
  }

  void _checkNext(CheckNext event, Emitter<RegisterState> emit) {
    emit(state.copyWith(
      canNext: check(),
    ));
  }

  // 检测是否可以直接注册
  void goRegister(BuildContext context, navigator) {
    add(GoRegister(context, navigator));
  }

  void _goRegister(GoRegister event, Emitter<RegisterState> emit) async {
    UserInfo? res = await registerData(state.emailText);
    if (res != null) {
      event.navigator.push(
        MaterialPageRoute(
          builder: (context) => const Home(),
        ),
      );
    } else {
      changeShowDialogTag(true);
    }
  }

  void changeShowDialogTag(bool tag) {
    add(ChangeShowDialogTag(tag));
  }

  void _changeShowDialogTag(
      ChangeShowDialogTag event, Emitter<RegisterState> emit) {
    emit(state.copyWith(
      showDialogTag: event.tag,
    ));
  }

  // 检测邮箱是否正确
  String? checkEmailError() {
    if (!isEmail(state.emailText.toString())) {
      return "Please enter the correct email";
    } else {
      return null;
    }
  }

  // 检测密码是否大于6位
  String? checkPassword() {
    if (state.passwordText.trim().isNotEmpty) {
      return state.passwordText.trim().length > 5
          ? null
          : "Password cannot be less than 6 digits";
    }
    return null;
  }

  // 检测确认密码是否大于6位
  String? checkConfirmPassword() {
    if (state.confirmPasswordText.isNotEmpty) {
      if (state.confirmPasswordText != state.passwordText) {
        return 'Inconsistent password input before and after';
      }
    }
    return null;
  }

  bool check() {
    bool tag = true;
    if (!isEmail(state.emailText)) {
      tag = false;
    }
    if (state.passwordText.length < 6) {
      tag = false;
    }
    if (state.confirmPasswordText != state.passwordText) {
      tag = false;
    }
    return tag;
  }
}

bool isEmail(String email) {
  email = email.toLowerCase();
  String reg = "^[A-Za-z0-9-._=+]+@[-a-z0-9._=+]+\\.\\w+\$";
  RegExp emailCode = RegExp(reg);
  return emailCode.hasMatch(email);
}
