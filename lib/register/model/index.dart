class UserInfo {
  UserInfo({
    required this.email,
    required this.id,
  });
  String? email;
  int? id;
  UserInfo.fromJson(Map<String, dynamic> json) {
    email = json['email'];
    id = json['id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['email'] = email;
    data['id'] = id;
    return data;
  }
}
