import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:register_demo/bloc/app_bloc.dart';
import 'package:register_demo/register/bloc/register_bloc.dart';

class Register extends StatefulWidget {
  const Register({Key? key}) : super(key: key);

  @override
  State<Register> createState() => _RegisterState();
}

class _RegisterState extends State<Register> {
  // email输入框控制器
  TextEditingController emailController = TextEditingController();
  // 密码输入框控制器
  TextEditingController passwordController = TextEditingController();
  // 确认密码输入框控制器
  TextEditingController confirmPasswordController = TextEditingController();
  // 表单key
  final GlobalKey _formKey = GlobalKey<FormState>();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AppBloc, AppState>(
      builder: (context, appState) {
        return BlocBuilder<RegisterBloc, RegisterState>(
            builder: (context, state) {
          return Scaffold(
            body: Stack(
              children: [
                Center(
                  child: Container(
                    width: 500,
                    height: 700,
                    padding: const EdgeInsets.symmetric(horizontal: 30),
                    decoration: const BoxDecoration(
                      color: Colors.white,
                    ),
                    child: Column(
                      children: [
                        Container(
                          margin: const EdgeInsets.only(bottom: 10, top: 10),
                          child: const Text(
                            'Register',
                            style: TextStyle(
                              fontSize: 30,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                        Form(
                          key: _formKey,
                          autovalidateMode: AutovalidateMode.onUserInteraction,
                          child: Column(
                            children: [
                              // 邮箱模块
                              Container(
                                margin: const EdgeInsets.only(bottom: 20),
                                child: TextFormField(
                                  controller: emailController,
                                  onChanged: (val) {
                                    context
                                        .read<RegisterBloc>()
                                        .setEmailText(val);
                                  },
                                  decoration: const InputDecoration(
                                    labelText: "Email",
                                    hintText: "Email",
                                    icon: Icon(Icons.email),
                                  ),
                                  // 校验邮箱
                                  validator: (v) {
                                    // 检测确认密码是否符合要求
                                    return context
                                        .read<RegisterBloc>()
                                        .checkEmailError();
                                  },
                                ),
                              ),
                              // 密码模块
                              Container(
                                margin: const EdgeInsets.only(bottom: 20),
                                child: TextFormField(
                                  controller: passwordController,
                                  decoration: const InputDecoration(
                                    labelText: "Password",
                                    hintText: "Password",
                                    icon: Icon(Icons.lock),
                                  ),
                                  obscureText: true,
                                  onChanged: (val) {
                                    context
                                        .read<RegisterBloc>()
                                        .setPassword(val);
                                  },
                                  //校验密码
                                  validator: (v) {
                                    return context
                                        .read<RegisterBloc>()
                                        .checkPassword();
                                  },
                                ),
                              ),
                              // 确认密码
                              Container(
                                margin: const EdgeInsets.only(bottom: 50),
                                child: TextFormField(
                                  controller: confirmPasswordController,
                                  decoration: const InputDecoration(
                                    labelText: "Confirm Password",
                                    hintText: "Confirm Password",
                                    icon: Icon(Icons.lock),
                                  ),
                                  obscureText: true,
                                  onChanged: (val) {
                                    context
                                        .read<RegisterBloc>()
                                        .setConfirmPassword(val);
                                  },
                                  //校验密码
                                  validator: (v) {
                                    // 检测确认密码是否符合要求
                                    return context
                                        .read<RegisterBloc>()
                                        .checkConfirmPassword();
                                  },
                                ),
                              ),
                            ],
                          ),
                        ),
                        state.canNext
                            ? InkWell(
                                child: Container(
                                  height: 50,
                                  alignment: Alignment.center,
                                  decoration: const BoxDecoration(
                                    color: Colors.blue,
                                    borderRadius: BorderRadius.all(
                                      Radius.circular(50),
                                    ),
                                  ),
                                  child: const Text(
                                    'Register',
                                    style: TextStyle(
                                      fontSize: 25,
                                      color: Colors.white,
                                    ),
                                  ),
                                ),
                                onTap: () {
                                  final navigator = Navigator.of(context);
                                  context
                                      .read<RegisterBloc>()
                                      .goRegister(context, navigator);
                                  context
                                      .read<AppBloc>()
                                      .setEmailText(state.emailText);
                                },
                              )
                            : Container(
                                height: 50,
                                alignment: Alignment.center,
                                decoration: const BoxDecoration(
                                  color: Colors.grey,
                                  borderRadius: BorderRadius.all(
                                    Radius.circular(50),
                                  ),
                                ),
                                child: const Text(
                                  'Register',
                                  style: TextStyle(
                                    fontSize: 25,
                                    color: Colors.white,
                                  ),
                                ),
                              ),
                      ],
                    ),
                  ),
                ),
                state.showDialogTag
                    ? Positioned(
                        left: 0,
                        top: 0,
                        child: showErrorAlert(),
                      )
                    : Container(),
              ],
            ),
          );
        });
      },
    );
  }

  Widget showErrorAlert() {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height,
      alignment: Alignment.center,
      color: const Color(0x66000000),
      child: Container(
        alignment: Alignment.center,
        width: 300,
        height: 150,
        padding: const EdgeInsets.only(left: 30, right: 30, top: 30),
        color: Colors.white,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            const Text(
              'This email address is already registered',
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: 20,
              ),
            ),
            InkWell(
              child: Container(
                height: 50,
                width: double.infinity,
                alignment: Alignment.center,
                decoration: const BoxDecoration(
                  border: Border(
                    top: BorderSide(width: 1, color: Colors.grey),
                  ),
                ),
                child: const Text(
                  'OK',
                  style: TextStyle(
                    fontSize: 20,
                  ),
                ),
              ),
              onTap: () {
                context.read<RegisterBloc>().changeShowDialogTag(false);
              },
            ),
          ],
        ),
      ),
    );
  }
}
